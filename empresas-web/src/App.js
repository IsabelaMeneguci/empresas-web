import React, {Component} from 'react'
import './style/App.css'
import { MuiThemeProvider } from '@material-ui/core/styles';
import theme from './theme'
import {Route} from 'react-router-dom'
import routerConfig from './RouterConfig'

class App extends Component {
  render(){
    return (
      <div className="app-class">
      <MuiThemeProvider theme={theme}>
      {routerConfig.map((value, key)=>{
        return <Route
          key={key}
          path={value.path}
          component={value.component}
          exact={value.exact}
        />
      })}
      </MuiThemeProvider>
      </div>
    );  
  }
  }

export default App;

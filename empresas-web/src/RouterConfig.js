import Login from './screens/Login'
import List from './screens/List'

const routesConfig = [
    {
      path: '/',
      component: Login,
      exact: true
    },
    {
      path: '/list',
      component: List,
      exact: true
    }
]

export default routesConfig
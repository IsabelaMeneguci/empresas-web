import React from 'react'
import clsx from 'clsx'
import { makeStyles } from '@material-ui/core/styles';
import {Button, Input, InputLabel, InputAdornment, IconButton, FormControl } from '@material-ui/core'
import {VisibilityOff, Visibility, MailOutline, LockOpen, Lock} from '@material-ui/icons'
import '../style/fonts.css'
import '../style/colors.css'
import '../style/App.css'


const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  margin: {
    margin: '10px',
  },
  withoutLabel: {
    marginTop: '50px',
  },
  textField: {
    width: '20ch',
  },
}));

export default function Formulario() {
  const classes = useStyles()
  const [values, setValues] = React.useState({
    password: '',
    email: '',
    showPassword: false,
  });


  const handleChange = (prop) => (event) => {
    setValues({ ...values, [prop]: event.target.value });
  };

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };
  return (
    <div>
    <FormControl className={clsx(classes.margin, classes.textField)}>
      <InputLabel htmlFor="standard-adornment-password">Email</InputLabel>
        <Input
          id="email"
          value={values.email}
          onChange={handleChange('email')}
          startAdornment={
            <InputAdornment position="start">
              <MailOutline color="primary"/>
            </InputAdornment>
          }
        />
    </FormControl>      
    <FormControl className={clsx(classes.margin, classes.textField)}>
      <InputLabel htmlFor="standard-adornment-password">Senha</InputLabel>
      <Input
        id="password"
        type={values.showPassword ? 'text' : 'password'}
        value={values.password}
        onChange={handleChange('password')}
        startAdornment={
          <InputAdornment position="start">
            {values.showPassword ? <LockOpen color="primary"/> : <Lock color="primary"/>}
          </InputAdornment>
        }
        endAdornment={
          <InputAdornment position="end">
            <IconButton
              color="primary"
              aria-label="toggle password visibility"
              onClick={handleClickShowPassword}
              onMouseDown={handleMouseDownPassword}
            >
              {values.showPassword ? <Visibility /> : <VisibilityOff />}
            </IconButton>
          </InputAdornment>
        }
      />
      </FormControl>
      <FormControl className={clsx(classes.margin, classes.textField)}>
          <Button 
            disabled={!values.email || !values.password}
            variant="contained"
            color="secondary"
            href="/list"
          >
            ENTRAR
          </Button>
      </FormControl>
    </div>
    );
}


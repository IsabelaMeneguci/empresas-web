import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import {Container, Toolbar, IconButton, Typography, AppBar, CssBaseline, Box} from '@material-ui/core'
import {ArrowBack, Search} from '@material-ui/icons'
import logo from '../static/logo-nav.png'
import '../style/fonts.css'
import '../style/colors.css'
import '../style/App.css'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  icon: {
    marginRight: theme.spacing(2),
    color: 'white'
  },
  title: {
    flexGrow: 1,
    marginTop: '5px'
  },
}));

export default function ButtonAppBar() {
  const classes = useStyles()
    return (
      <React.Fragment>
        <CssBaseline />
          <AppBar>
            <Toolbar className="toolbar-class">
              <IconButton edge="start" className={classes.icon} aria-label="back">
                <ArrowBack />
              </IconButton>
              <Typography variant="h6" className={classes.title}>
                <img src={logo} alt="Logo" />
              </Typography>
              <IconButton aria-label="search" className={classes.icon} >
                <Search />
              </IconButton>
            </Toolbar>
          </AppBar>
        <Toolbar />
        <Container>
        <Box>
          <span className="Text-Style-3 login-class">Clique na busca para iniciar.</span>
        </Box>
        </Container>
    </React.Fragment>
  );
}



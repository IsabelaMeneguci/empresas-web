import React, {Component} from 'react'
import Formulario from '../components/Formulario'
import logo from '../static/logo-home.png'
import '../style/fonts.css'
import '../style/colors.css'
import '../style/App.css'


class Login extends Component {
  render() {
    return (
        <div className="login-class mdc-layout-grid">
          <div className="mdc-layout-grid__inner">
            <div className="mdc-layout-grid__cell margin-bottom-lg ">
              <img alt="Logo IOASYS" src={logo}/>
            </div>
            <div className="mdc-layout-grid__cell size-col-1">
              <span className="Text-Style-11">BEM-VINDO AO EMPRESAS</span>
            </div>
            <div className="mdc-layout-grid__cell size-col-2">
              <span className="Text-Style">
                Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.
              </span>
            </div>
            <div className="mdc-layout-grid__cell size-col-2">
              <Formulario />
            </div>
          </div>
        </div>
      );
  }
}
export default Login


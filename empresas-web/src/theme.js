import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#ee4c77',
      main: '#ee4c77',
      dark: '#ee4c77',
      contrastText: '#ffffff',
    },
    secondary: {
      light: '#57bbbc',
      main: '#57bbbc',
      dark: '#57bbbc',
      contrastText: '#ffffff',
    },
  }
});

export default theme;